package dagger.http.cookie;

public interface CookieOption {

    String getValue();

}
